# as builder implies "Builder Phase"
FROM node:alpine as builder

WORKDIR '/app'

COPY package.json .
RUN npm install

COPY . .

RUN npm run build

# will build to /app/build

# every phase can only have ONE 'FROM' statement
FROM nginx

# copy over something from the phase we were just working on
COPY --from=builder /app/build /usr/share/nginx/html